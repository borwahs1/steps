class Constants
  HIGH_STEPS_KEY = 'high_steps'
  HIGH_STEPS_DATE_KEY = 'high_steps_date'
  LOW_STEPS_KEY = 'low_steps'
  LOW_STEPS_DATE_KEY = 'low_steps_date'
  AVERAGE_STEPS_KEY = 'average_steps'

  STEPS_DATA_KEY = 'steps_data'
  LAST_UPDATED_KEY = 'last_updated'

  ALL_HIGH_STEPS_KEY = 'all_high_steps'
  ALL_HIGH_STEPS_DATE_KEY = 'all_high_steps_date'
  ALL_LOW_STEPS_KEY = 'all_low_steps'
  ALL_LOW_STEPS_DATE_KEY = 'all_low_steps_date'

  CONFIG_FILE = "config.yml"
  STEPS_SETTS = "opts"
  PASS_KEY = "password"
  GRAPH_DAYS_KEYS = "graph_days"

  REDIS_OPTS = "redis"
  SERVER_KEY = "server"
  PORT_KEY = "port"

  NIKEPLUS_DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
  CHART_DATETIME_FORMAT = "%m/%d"
  MDY_DATETIME_FORMAT = "%m/%d/%y"
  LAST_UPDATED_DATETIME_FORMAT = "%m/%d/%y %I:%M %p"
  BASE_URL = "https://api.nike.com".freeze
  ENDPOINT = "/me/sport/activities".freeze

  NIKEPLUS_OPT = "nikeplus"
  APP_ID_KEY = "app_id"
  ACCESS_TOKEN_KEY = "access_token"
end
require 'rubygems'
require 'bundler'

Bundler.require

# require constants
require './constants.rb'

# require update steps worker (resque)
require './update_steps.rb'
require 'resque/tasks'

require 'date'
require 'json'
require 'open-uri'
require 'uri'
require 'yaml'

task :default => :fetch_stats

def open_redis_connection(host, port)
  redis = nil

  begin
    redis = Redis.new(host: host, port: port, password: nil)
    redis.ping
  rescue
    redis = nil
  end

  redis
end

def get_nikeplus_steps_data(access_token, app_id, count, offset)
  access_token_qs = "?access_token=#{access_token}"

  url = "#{Constants::BASE_URL}#{Constants::ENDPOINT}#{access_token_qs}"
  url << "&count=#{count}&offset=#{offset}"

  json_result = ""

  open(url, 'appid' => app_id, 'Accept' =>'application/json') do |f|
    response = f.read

    json_result = JSON.parse(response)
  end

  json_result
end

def set_all_time_high_low_stats(redis, key_values, high_steps, low_steps, high_step_date, low_step_date)
  curr_low_steps = redis[Constants::ALL_LOW_STEPS_KEY].to_i
  curr_high_steps = redis[Constants::ALL_HIGH_STEPS_KEY].to_i

  curr_utc_time = DateTime.now.new_offset(0).strftime(Constants::MDY_DATETIME_FORMAT)

  if curr_low_steps == 0 or curr_low_steps > low_steps

    ls_date = low_step_date.strftime(Constants::MDY_DATETIME_FORMAT)

    if not ls_date.eql? curr_utc_time
      key_values[Constants::ALL_LOW_STEPS_KEY] = low_steps
      key_values[Constants::ALL_LOW_STEPS_DATE_KEY] = ls_date
    end
  end

  if curr_high_steps == 0 or curr_high_steps < high_steps

    hs_date = high_step_date.strftime(Constants::MDY_DATETIME_FORMAT)

    if not hs_date.eql? curr_utc_time
      key_values[Constants::ALL_HIGH_STEPS_KEY] = high_steps
      key_values[Constants::ALL_HIGH_STEPS_DATE_KEY] = hs_date
    end
  end

  key_values
end

def build_steps_data_from_response(data)
  stats = Hash.new
  stats_arr = Array.new

  data.each do |activity|
    stat = Hash.new

    stepDate = Date.strptime(activity["startTime"], Constants::NIKEPLUS_DATETIME_FORMAT)

    stat[:date] = stepDate
    stat[:steps] = activity["metricSummary"]["steps"]

    stats_arr.push(stat)
  end

  stats[:stats] = stats_arr
end

# example:
# {
#   labels : ["09/20", "09/21", "09/22", "09/23", "09/24"],
#   datasets : [
#     {
#       fillColor : "rgba(220,220,220,0.5)",
#       strokeColor : "rgba(220,220,220,1)",
#       pointColor : "rgba(220,220,220,1)",
#       pointStrokeColor : "#fff",
#       data : [8654,7612,12312,10012,9874]
#     }
#   ]
# }
def build_steps_data(redis, steps_data)
  key_values = Hash.new

  high_steps = 0
  high_step_date = ""
  low_steps = -1
  low_step_date = ""

  labels = Array.new
  data = Array.new

  total_steps = 0
  steps_data.each do |step_data|
    steps = step_data[:steps]

    step_date = step_data[:date].strftime(Constants::CHART_DATETIME_FORMAT)

    if steps > high_steps and steps != 0
      high_steps = steps
      high_step_date = step_data[:date]
    end

    if (low_steps == -1 or steps < low_steps) and steps != 0
      low_steps = steps
      low_step_date = step_data[:date]
    end

    labels.insert(0, step_date)
    data.insert(0, steps)

    total_steps = total_steps + steps
  end

  average_steps = total_steps / steps_data.count

  datasets = Array.new
  dataset = Hash.new
  dataset['fillColor'] = "rgba(220,220,220,0.5)"
  dataset['strokeColor'] = "rgba(220,220,220,1)"
  dataset['pointColor'] = "rgba(220,220,220,1)"
  dataset['pointStrokeColor'] = "#ffffff"
  datasets.push(dataset)

  dataset['data'] = data

  chart_data = Hash.new
  chart_data['labels'] = labels
  chart_data['datasets'] = datasets

  key_values[Constants::HIGH_STEPS_KEY] = "#{high_steps}"
  key_values[Constants::HIGH_STEPS_DATE_KEY] = high_step_date.strftime(Constants::MDY_DATETIME_FORMAT)
  key_values[Constants::LOW_STEPS_KEY] = "#{low_steps}"
  key_values[Constants::LOW_STEPS_DATE_KEY] = low_step_date.strftime(Constants::MDY_DATETIME_FORMAT)
  key_values[Constants::AVERAGE_STEPS_KEY] = average_steps

  redis[Constants::STEPS_DATA_KEY] = chart_data.to_json
  redis[Constants::LAST_UPDATED_KEY] = Time.now.strftime(Constants::LAST_UPDATED_DATETIME_FORMAT)

  set_all_time_high_low_stats(redis, key_values, high_steps, low_steps, high_step_date, low_step_date)

  store_redis_key_values(redis, key_values)
end

def store_redis_key_values(redis, key_values)
  key_values.each do |key, value|
    redis[key] = value
  end
end

def validation(access_token, app_id)
  valid = true

  if access_token.nil? or access_token.empty?
    valid = false
    p "Access Token is empty"
  end

  if app_id.nil? or app_id.empty?
    valid = false
    p "App ID is empty"
  end

  valid
end

def reset_all_time_stats(redis)
  redis[Constants::ALL_LOW_STEPS_KEY] = 0
  redis[Constants::ALL_LOW_STEPS_DATE_KEY] = ''
  redis[Constants::ALL_HIGH_STEPS_KEY] = 0
  redis[Constants::ALL_HIGH_STEPS_DATE_KEY] = ''
end

def create_redis_connection(host, port)
  redis = open_redis_connection(host, port)
  if redis.nil?
    p "Cannot connect to Redis."
    return false
  end

  redis
end

def load_config
  base_directory = File.dirname(__FILE__)
  config_file = File.join(base_directory, Constants::CONFIG_FILE)
  config = YAML::load_file(config_file)

  config
end

task :reset_all_time_stats do
  config = load_config

  host = config[Constants::REDIS_OPTS][Constants::SERVER_KEY]
  port = config[Constants::REDIS_OPTS][Constants::PORT_KEY]

  redis = create_redis_connection(host, port)

  if redis
    reset_all_time_stats(redis)
  end
end

desc "Fetch Stats"
task :fetch_stats  do
  config = load_config

  host = config[Constants::REDIS_OPTS][Constants::SERVER_KEY]
  port = config[Constants::REDIS_OPTS][Constants::PORT_KEY]

  graph_days = config[Constants::STEPS_SETTS][Constants::GRAPH_DAYS_KEYS]

  access_token = config[Constants::NIKEPLUS_OPT][Constants::ACCESS_TOKEN_KEY]
  app_id = config[Constants::NIKEPLUS_OPT][Constants::APP_ID_KEY]

  if (validation(access_token, app_id))
    redis = create_redis_connection(host, port)

    if redis
      nike_plus_data = get_nikeplus_steps_data(access_token, app_id, graph_days, 1)

      data = build_steps_data_from_response(nike_plus_data["data"])

      build_steps_data(redis, data)
    end
  end
end

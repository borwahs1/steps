require 'rubygems'
require 'bundler'

Bundler.require

require 'yaml'
require './constants.rb'

base_directory = File.dirname(__FILE__)
config_file = File.join(base_directory, Constants::CONFIG_FILE)
config = YAML::load_file(config_file)

host = config[Constants::REDIS_OPTS][Constants::SERVER_KEY]
port = config[Constants::REDIS_OPTS][Constants::PORT_KEY]

$redis = Redis.new(host: host, port: port, password: nil)

require './steps'
run Steps
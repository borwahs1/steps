require 'yaml'

class Steps < Sinatra::Application

  # require model classes
  Dir[File.dirname(__FILE__) + '/update_steps.rb'].each {|file| require file }

  # require constants file
  require './constants.rb'

  # setup Resque with current redis connection
  Resque.redis = Redis::Namespace.new(:resque, :redis => $redis)

  set :base_directory, File.dirname(__FILE__)
  set :config_file, File.join(settings.base_directory, Constants::CONFIG_FILE)
  set :config_opts, YAML::load_file(settings.config_file)
  set :password, settings.config_opts[Constants::STEPS_SETTS][Constants::PASS_KEY]

  # Helper Methods

  def number_comma(number, options = {})
    number.to_s.chars.to_a.reverse.each_slice(3).map(&:join).join(",").reverse
  end

  # Routes

  get '/' do
    @high_steps = number_comma($redis[Constants::HIGH_STEPS_KEY])
    @high_steps_date = $redis[Constants::HIGH_STEPS_DATE_KEY]
    @low_steps = number_comma($redis[Constants::LOW_STEPS_KEY])
    @low_steps_date = $redis[Constants::LOW_STEPS_DATE_KEY]
    @average_steps = number_comma($redis[Constants::AVERAGE_STEPS_KEY])

    @steps = $redis[Constants::STEPS_DATA_KEY]
    @last_updated = $redis[Constants::LAST_UPDATED_KEY]

    @all_high_steps = number_comma($redis[Constants::ALL_HIGH_STEPS_KEY])
    @all_high_steps_date = $redis[Constants::ALL_HIGH_STEPS_DATE_KEY]
    @all_low_steps = number_comma($redis[Constants::ALL_LOW_STEPS_KEY])
    @all_low_steps_date = $redis[Constants::ALL_LOW_STEPS_DATE_KEY]

    slim :index
  end

  get '/update/?' do
    slim :update
  end

  post '/update/steps/?' do
    password = params[:password]
    env_pass = settings.password

    if password.nil? or password.empty? or env_pass.nil? or env_pass.empty?
      redirect '/update/'
    end

    if password.eql? env_pass
      enqueued = Resque.enqueue(UpdateSteps)

      if enqueued
        redirect '/'
      end
    end

    redirect '/update/'
  end
end